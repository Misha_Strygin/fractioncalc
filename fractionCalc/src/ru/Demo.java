import java.util.Scanner;

public class Demo {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Напишите ваше выражение так, чтобы знак операчии окружал пробел, пример:5/2 + 6/2");
        String expression = scanner.nextLine();
        String[] symbols = expression.split(" ");
        String[] op1 = symbols[0].split("/");
        String[] op2 = symbols[2].split("/");
        Fraction f1 = new Fraction(Integer.valueOf(op1[0]),Integer.valueOf(op1[1]));
        Fraction f2 = new Fraction(Integer.valueOf(op2[0]),Integer.valueOf(op2[1]));
        Fraction f3;
        switch (symbols[1]){
            case "+":
                f3 = Calc.add(f1,f2);
                fractionDo(f3);
                break;
            case "-":
                f3 = Calc.subtraction(f1, f2);
                fractionDo(f3);
                break;
            case "*":
                f3 = Calc.multiplication(f1,f2);
                fractionDo(f3);
                break;
            case "/":
                f3 = Calc.division(f1,f2);
                fractionDo(f3);
                break;
                default:
                    System.out.println("Похоже вы ввели некоректный знак операции");
                    break;

        }
    }

    private static void fractionDo(Fraction f3) {
        f3 = Calc.reductionOfFraction(f3);
        Fraction.FractionToString(f3);
    }

}