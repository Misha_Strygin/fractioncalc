public class Fraction {
    public int denominator;
    public int numerator;
    public int wholePart;


    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction() {
        this(1, 1);
    }


    public static void FractionToString(Fraction f1) {
        if (f1.denominator == f1.numerator)System.out.printf("Результат выражения равен 1%n");
        else if (f1.denominator == 1)System.out.printf("Результат выражения равен %d %n", f1.numerator);
        else if(f1.numerator%f1.denominator==0)System.out.printf("Результат выражения равен %d %n", f1.numerator/f1.denominator );
        else
        System.out.printf("Результат выражения равен %d/%d %n", f1.numerator, f1.denominator);
    }
}

